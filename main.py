import usbtemp
import time
import os
import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))


# List all serial ports
temp_sensor_ports = []
temp_sensors = {}

## Check all USB devices
import serial.tools.list_ports

ports = serial.tools.list_ports.comports()
for port, desc, hwid in sorted(ports):
    print("{}: {} [{}]".format(port, desc, hwid))
    if "USB VID:PID=067B:2303" in hwid:
        temp_sensor_ports.append(port)
        print("\tThis device was added")

## Open all temperature sensors
for port in temp_sensor_ports:
    temp_sensor = usbtemp.Thermometer(port)
    temp_sensor.Open()
    temp_sensors[temp_sensor.Rom()] = temp_sensor

# Set up and connect to broker
client = mqtt.Client()
client.on_connect = on_connect
if os.environ.get("MQTT_USERNAME"):
    client.username_pw_set(os.environ["MQTT_USERNAME"], os.environ["MQTT_PASSWORD"])
client.connect(os.environ["MQTT_BROKERADDR"], int(os.environ["MQTT_BROKERPORT"]), 60)
client.loop_start()

# Sample sensors
sample_period_s = int(os.environ.get("SAMPLE_PERIOD_S", 30))
if sample_period_s < 10:
    sample_period_s = 10
time_next = time.time()
while True:
    print("Sampling sensors!")
    for temp_sensor_id in temp_sensors:
        client.publish(
            f"DaC/usbtemp/temperature/{temp_sensor_id}",
            temp_sensors[temp_sensor_id].Temperature(),
        )
    time_next = time_next + sample_period_s

    # Sleep desired time
    time.sleep(time_next - time.time())

client.loop_stop(force=False)
