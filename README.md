# usbtemp2mqtt

 This application samples a number of usb temperature sensors (https://usbtemp.com/) and sends their data over MQTT

 ## sample docker-compose file


```
version: "3"

services:
  sampler:
    image: registry.gitlab.com/erik.osvaldsson/usbtemp2mqtt
    environment:
      MQTT_BROKERADDR: X
      MQTT_BROKERPORT: Y
      MQTT_USERNAME: mqtt username
      MQTT_PASSWORD: mqtt password
      SAMPLE_PERIOD_S: 30
    privileged: true
    volumes:
    - /dev:/dev
```