FROM python:3

WORKDIR /usr/src/app

COPY Pipfile .
COPY Pipfile.lock .
RUN pip install pipenv && pipenv install --system --deploy
COPY *.py ./

CMD [ "python", "./main.py" ]